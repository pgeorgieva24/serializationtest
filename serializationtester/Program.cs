﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace serializationtester
{

    public enum MessageTypes
    {
        Unknown = 0,
        SendMesssage = 1,
        DeleteMessage = 2,
        Login = 3,
        RequestMessageList = 4,
        RequestMessage = 5,
        ResponseToken = 6,
        ResponseMessagePacket = 7,
        IncorrectCredentials = 8,
        UsernameExists = 9,
        RegisterSelf = 10,
        Test = 11
    }

    class Program
    {
        /// <summary>
        /// [0] size of bytes
        /// [1] message type
        /// [2] size of first property
        /// [3 - size of first property] property ...
        /// and so on
        /// for now check if property type is string, datetime or int and handle it acordingly and we have to do: list of string and file
        /// we get the properties oredred by attribute ParameterOrder so we always know the order of properties to serialize and deserialize
        /// if we convert the message type we dont need to say its size; its fixed 1 byte size
        /// all propertties are turned into byte[] and added to list<byte[]> and send to method Combine to make one big array 
        /// if string symbols count >255 wont work correct we'll think how to fix
        /// byte[0] is for size of all other bytes and don't know if needed
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSerialize"></param>
        /// <returns>byte array </returns>
        public static byte[] Serialize<T>(T objectToSerialize) where T : BaseTest
        {
            List<byte[]> byteArrays = new List<byte[]>();

            MessageTypes messageType;
            if (!Enum.TryParse(typeof(T).Name, out messageType))
                throw new NullReferenceException("There is no such a command");

            //in the final array should be in the second index [2]
            byteArrays.Add(new byte[] { Convert.ToByte(messageType) });

            var propertyInfos = objectToSerialize.GetType().GetProperties();

            foreach (var property in propertyInfos)
            {
                byte[] tempArray = null;
                object propValue = property.GetValue(objectToSerialize);
                string propTypeName = property.PropertyType.Name;

                if (propTypeName.Equals(typeof(string).Name))
                {
                    tempArray = Encoding.UTF8.GetBytes((string)propValue);
                }
                else if (propTypeName.Equals(typeof(DateTime).Name))
                {
                    tempArray = BitConverter.GetBytes(Convert.ToDateTime(propValue).ToBinary());
                }
                else if (propTypeName.Equals(typeof(int).Name))
                {
                    tempArray = new byte[] { (byte)propValue };
                }
                else if (propTypeName.Equals(typeof(List<string>).Name))
                {
                    List<byte[]> listByteArray = new List<byte[]>();

                    List<string> listOfStrings = (List<string>)propValue;

                    foreach (var item in listOfStrings)
                    {
                        var itemByteArray = Encoding.UTF8.GetBytes(item);
                        listByteArray.Add(GetBinarySizeOfProperty(itemByteArray.Length));
                        listByteArray.Add(itemByteArray);
                    }
                    tempArray = Combine(listByteArray);
                }
                else continue;


                //when deserializing we have to explixitly know which properties {sizes} are gonna be bigger than 255
                byteArrays.Add(GetBinarySizeOfProperty(tempArray.Length, property.Name));

                Console.WriteLine("property size is " + string.Join("--", byteArrays.Last()) + " property name " + property.Name + " : ");


                byteArrays.Add(tempArray);

                Console.WriteLine(string.Join(", ", tempArray));
            }

            return Combine(byteArrays, true);
        }

        /// <summary>
        /// if we send property named Message or ListOfNames we give it 2 bytes for size of property
        /// else only one byte
        /// </summary>
        private static byte[] GetBinarySizeOfProperty(int arrayLength, string propertyName = "")
        {
            if (propertyName.ToLower().Equals("message") || propertyName.ToLower().Equals("listofnames"))
                return BitConverter.GetBytes((ushort)arrayLength);
            else
                return new byte[] { (byte)arrayLength };
        }

        /// <summary>
        /// returns all arrays combined to one big array; can be added size of array in first 2 bytes
        /// </summary>
        private static byte[] Combine(List<byte[]> arrays, bool shouldCalculateTotalSizeOfArray = false)
        {
            var totalArraysLength = shouldCalculateTotalSizeOfArray ? arrays.Sum(a => a.Length) + 2 : arrays.Sum(a => a.Length);

            int offset = 0;
            byte[] rv = new byte[totalArraysLength];
            if (shouldCalculateTotalSizeOfArray)
            {
                var binaryArrayLength = BitConverter.GetBytes((ushort)totalArraysLength);
                Array.Copy(binaryArrayLength, rv, binaryArrayLength.Length);
                offset += 2;
            }

            foreach (byte[] array in arrays)
            {
                Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }

            return rv;
        }

        /// <summary>
        /// gets an array and returns object that inherits BaseTest
        /// gets message type and creates instance of class with same name
        /// </summary>
        public static T Deserialize<T>(byte[] bytesToDeserialize) where T : BaseTest
        {
            ushort expextedLength = (ushort)BitConverter.ToInt16(bytesToDeserialize.Take(2).ToArray(), 0);
            if (expextedLength > bytesToDeserialize.Length)
            {
                throw new Exception("not whole message received");
            }

            Type[] types = Assembly.GetExecutingAssembly().GetTypes().Where(t => typeof(BaseTest).IsAssignableFrom(t)).ToArray();

            string InvalidCommandNameExceptionMessage = "Type '{0}' doesn't exists!";

            try
            {
                var messagetype = ((MessageTypes)bytesToDeserialize[2]).ToString();

                Type type = types.FirstOrDefault(ct => ct.Name.Equals(messagetype, StringComparison.OrdinalIgnoreCase));

                if (type == null)
                {
                    throw new ArgumentException(string.Format(InvalidCommandNameExceptionMessage, messagetype.ToString()));
                }

                object instance = Activator.CreateInstance(type);
                var propertyInfos = instance.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

                int currentIndex = 3;
                int propertyLength;

                foreach (var property in propertyInfos)
                {
                    string propTypeName = property.PropertyType.Name;


                    //for now messges and List of string can have more than 255 symbols
                    if (property.Name.Equals("Message") || propTypeName.Equals(typeof(List<string>).Name))
                    {
                        byte[] a = new byte[2];
                        Array.Copy(bytesToDeserialize, currentIndex, a, 0, 2);
                        propertyLength = (ushort)BitConverter.ToInt16(a, 0);
                        currentIndex += 2;
                    }
                    else propertyLength = bytesToDeserialize[currentIndex++];

                    byte[] tempArray = new byte[propertyLength];
                    Array.Copy(bytesToDeserialize, currentIndex, tempArray, 0, propertyLength);
                    currentIndex += propertyLength;
                    
                    if (property.PropertyType.Name.Equals(typeof(string).Name))
                    {
                        property.SetValue(instance, Encoding.UTF8.GetString(tempArray));
                    }
                    else if (property.PropertyType.Name.Equals(typeof(DateTime).Name))
                    {
                        property.SetValue(instance, DateTime.FromBinary(BitConverter.ToInt64(tempArray, 0)));
                    }
                    else if (property.PropertyType.Name.Equals(typeof(int).Name))
                    {
                        property.SetValue(instance, (ushort)BitConverter.ToInt16(tempArray, 0));
                    }
                    else if (propTypeName.Equals(typeof(List<string>).Name))
                    {
                        List<string> propertyElements = new List<string>();
                        int elementPointer = 0;
                        int elementLength = 0;
                        do
                        {
                            elementLength = Convert.ToInt16(tempArray[elementPointer]);
                            elementPointer++;
                            byte[] element = new byte[elementLength];
                            Array.Copy(tempArray, elementPointer, element, 0, elementLength);
                            propertyElements.Add(Encoding.UTF8.GetString(element));
                            elementPointer += elementLength;
                        }
                        while (elementPointer != tempArray.Length);
                        property.SetValue(instance, propertyElements);

                    }
                    else throw new Exception("i dont know how to deserialize property of this type " + propTypeName);
                }

                return (T)instance;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        static void Main(string[] args)
        {
            Test t = new Test("alfonso gutierez", "plamena georgieva", @"A chat room is where dedicated group of users can communicate. ");

            byte[] array = Serialize(t);
            Console.WriteLine();

            Console.WriteLine("properties to serialize");
            Console.WriteLine(t.GetType().ToString());
            foreach (var prop in t.GetType().GetProperties())
            {
                Console.WriteLine(prop.GetValue(t).ToString());
            }
            t.ListOfNames.ForEach(x => Console.WriteLine(x));

            Console.WriteLine(string.Join(" - ", array));

            Console.WriteLine("--------------------");

            var testDeserialized = Deserialize<Test>(array);

            Console.WriteLine("properties to deserialize");
            Console.WriteLine(testDeserialized.GetType().ToString());
            foreach (var prop in testDeserialized.GetType().GetProperties())
            {
                Console.WriteLine(prop.GetValue(testDeserialized).ToString()+"="+prop.Name);
            }
            testDeserialized.ListOfNames.ForEach(x => Console.WriteLine(x));


            Console.WriteLine();
            Console.ReadLine();
        }
    }


    public class BaseTest
    {
        public string Receiver { get; set; }
        public string Name { get; set; }
    }

    class Test : BaseTest
    {

        public string Message { get; set; }
        public List<string> ListOfNames { get; set; }
        public DateTime Timestamp { get; set; }

        public Test() { }

        public Test(string name, string receiver, string message)
        {
            Name = name;
            Receiver = receiver;
            Message = message;
            Timestamp = DateTime.Now;
            ListOfNames = new List<string>() { "first", "second", "third" };
        }
    }

}
